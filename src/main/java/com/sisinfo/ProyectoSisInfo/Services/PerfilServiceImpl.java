package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Users;
import com.sisinfo.ProyectoSisInfo.Respositories.PerfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PerfilServiceImpl implements PerfilService {

    private PerfilRepository perfilRepository;

    @Autowired
    @Qualifier(value = "perfilRepository")
    public void setPerfilRepository(PerfilRepository perfilRepository){
        this.perfilRepository = perfilRepository;
    }

    @Override
    public Iterable<Perfil> listAllPerfiles() {
        return perfilRepository.findAll();
    }

    @Override
    public void savePerfil(Perfil perfil) {
        perfilRepository.save(perfil);
    }

    @Override
    public Perfil getPerfil(Integer id) {
        return perfilRepository.findOne(id);
    }

    @Override
    public void deletePerfil(Integer id) {
        perfilRepository.delete(id);
    }

    @Override
    public Iterable<Perfil> listAllPerfilesByUser(Users user){
        return perfilRepository.findAllById(user);
    }
}
