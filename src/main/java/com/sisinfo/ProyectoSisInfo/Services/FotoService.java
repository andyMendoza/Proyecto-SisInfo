package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Foto;
import com.sisinfo.ProyectoSisInfo.Entities.Registro;

public interface FotoService {

    Iterable<Foto> listAllFoto();
    void saveFoto(Foto foto);

    Foto getFoto(Integer id);

    void deleteFoto(Integer id);

    Iterable<Foto> listAllFotosById(Registro registro);

}
