package com.sisinfo.ProyectoSisInfo.Services;


import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Recordatorio;
import com.sisinfo.ProyectoSisInfo.Entities.Users;
import com.sisinfo.ProyectoSisInfo.Respositories.RecordatorioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class RecordatorioServiceImpl implements RecordatorioService{

    private RecordatorioRepository recordatorioRepository;

    @Autowired
    @Qualifier(value = "recordatorioRepository")
    public void setRecordatorioRepository(RecordatorioRepository recordatorioRepository){
        this.recordatorioRepository=recordatorioRepository;
    }

    @Override
    public Iterable<Recordatorio> listAllRecordatorios() {
        return recordatorioRepository.findAll();
    }

    @Override
    public void saveRecordatorio(Recordatorio recordatorio) {
        recordatorioRepository.save(recordatorio);
    }

    @Override
    public Recordatorio getRecordatorio(Integer id) {
        return recordatorioRepository.findOne(id);
    }

    @Override
    public void deleteRecordatorio(Integer id) {
        recordatorioRepository.delete(id);
    }

    @Override
    public Iterable<Recordatorio> listAllRecordatoriosByProfile(Perfil perfil){
        return recordatorioRepository.findAllByProfile(perfil);
    }
    @Override
    public ArrayList<String> listAllCitasAlarm(Users user){
        return recordatorioRepository.findAllCitasAlarm(user);
    }
    @Override
    public ArrayList<String> listAllCitasAlarmMotivo(Users user){
        return recordatorioRepository.findAllCitasAlarmMotivo(user);
    }
}
