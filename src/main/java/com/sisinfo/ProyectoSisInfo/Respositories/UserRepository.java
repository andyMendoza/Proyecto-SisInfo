package com.sisinfo.ProyectoSisInfo.Respositories;

import com.sisinfo.ProyectoSisInfo.Entities.Users;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends CrudRepository<Users, Integer> {

    Users findByUsername(String username);

}