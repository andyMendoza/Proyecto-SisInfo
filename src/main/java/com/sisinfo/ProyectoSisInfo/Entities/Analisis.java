package com.sisinfo.ProyectoSisInfo.Entities;

import javax.validation.constraints.NotNull;
import javax.persistence.*;

@Entity
public class Analisis extends Registro {

    @NotNull
    private String tipoDeAnalisis;

    public Analisis(){
        this.tipo = "Analisis";
    }

    public String getTipoDeAnalisis() {
        return tipoDeAnalisis;
    }

    public void setTipoDeAnalisis(String tipoDeAnalisis) {
        this.tipoDeAnalisis = tipoDeAnalisis;
    }
}
