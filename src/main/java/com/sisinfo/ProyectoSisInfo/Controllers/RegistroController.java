package com.sisinfo.ProyectoSisInfo.Controllers;

import com.google.common.collect.Iterables;
import com.sisinfo.ProyectoSisInfo.Entities.*;
import com.sisinfo.ProyectoSisInfo.Services.*;

import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class  RegistroController {

    private ConsultaService consultaService;
    private DiagnosticoService diagnosticoService;
    private AnalisisService analisisService;
    private PerfilService perfilService;
    private UserService userService;
    private FotoService fotoService;
    private RecordatorioService recordatorioService;
    private String username;

    @Autowired
    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    @Autowired
    public void setDiagnosticoService(DiagnosticoService diagnosticoService) { this.diagnosticoService = diagnosticoService; }

    @Autowired
    public void setAnalisisService(AnalisisService analisisService) {
        this.analisisService = analisisService;
    }

    @Autowired
    public void setPerfilService(PerfilService perfilService) {
        this.perfilService = perfilService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setFotoService(FotoService fotoService) {
        this.fotoService = fotoService;
    }

    @Autowired
    public void setRecordatorioService(RecordatorioService recordatorioService) {
        this.recordatorioService = recordatorioService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String list(Model model, @RequestParam(value = "perfil", required = false) Perfil perfilActual) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Iterable<Perfil> perfiles = perfilService.listAllPerfilesByUser(userService.findByUsername(this.username));
        if(!Iterables.isEmpty(perfiles)){
            if(perfilActual == null){
                perfilActual = perfiles.iterator().next();
            }
            Iterable<Registro> registroLista = new ArrayList<>();
            Iterable<Consulta> consultaLista = consultaService.listAllConsultasByProfile(perfilActual);
            Iterable<Diagnostico> diagnosticoLista = diagnosticoService.listAllDiagnosticosByProfile(perfilActual);
            Iterable<Analisis> analisisLista = analisisService.listAllAnalisisByProfile(perfilActual);
            for(Registro registro:consultaLista){
                ((ArrayList<Registro>) registroLista).add(registro);
            }
            for(Registro registro:diagnosticoLista){
                ((ArrayList<Registro>) registroLista).add(registro);
            }
            for(Registro registro:analisisLista){
                ((ArrayList<Registro>) registroLista).add(registro);
            }
            ((ArrayList<Registro>) registroLista).sort((d1, d2) -> d1.getFecha().compareTo(d2.getFecha()));
            Collections.reverse(((ArrayList<Registro>) registroLista));
            model.addAttribute("perfiles", perfiles);
            model.addAttribute("registroLista",registroLista);
            model.addAttribute("perfilActual", perfilActual);
            return "home";
        }else{
            return "redirect:/nuevoPerfil";
        }
    }

    @RequestMapping(value = "/eliminarImagenes/{id}")
    public String list(Model model, @PathVariable Integer id, @RequestParam("tipo") String tipo)throws UnsupportedEncodingException {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        List<Foto> fotos = new ArrayList<>();
        switch (tipo){
            case "Consulta":
                fotos = (List<Foto>)fotoService.listAllFotosById(consultaService.getConsulta(id));
                break;
            case "Analisis":
                fotos = (List<Foto>)fotoService.listAllFotosById(analisisService.getAnalisis(id));
                break;
            case "Diagnostico":
                fotos = (List<Foto>)fotoService.listAllFotosById(diagnosticoService.getDiagnosticoById(id));
                break;
        }
        byte[] encodeBase64;
        String base64Encoded;
        for(int i=0;i<fotos.size();i++)
        {
            encodeBase64 = Base64.encode(fotos.get(i).getFoto());
            base64Encoded = new String(encodeBase64,"UTF-8");
            fotos.get(i).setAuxEncoded(base64Encoded);
        }
        model.addAttribute("id", id);
        model.addAttribute("fotos", fotos);
        model.addAttribute("tipo", tipo);
        return "eliminarImagenes";
    }
    @RequestMapping(value = "/eliminarImagenes/{id}/{imgId}")
    public String listq(Model model, @PathVariable Integer id,@RequestParam("tipo") String tipo,@PathVariable String imgId, RedirectAttributes redirectAttributes)throws UnsupportedEncodingException {
        fotoService.deleteFoto(Integer.parseInt(imgId));
        redirectAttributes.addAttribute("tipo", tipo);
        return "redirect:/eliminarImagenes/" + id;
    }
    @RequestMapping(value = "/verImagen/{id}")
    public String verImagen(Model model, @PathVariable Integer id)throws UnsupportedEncodingException {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Foto foto = fotoService.getFoto(id);
        byte[] encodeBase64;
        String base64Encoded;
        encodeBase64 = Base64.encode(foto.getFoto());
        base64Encoded = new String(encodeBase64,"UTF-8");
        foto.setAuxEncoded(base64Encoded);
        model.addAttribute("foto", foto);
        return "verImagen";
    }
}
